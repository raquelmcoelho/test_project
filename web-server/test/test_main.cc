#define DROGON_TEST_MAIN
#include <drogon/drogon_test.h>
#include <drogon/drogon.h>

using namespace drogon;

DROGON_TEST(MainRouteTest)
{
    auto client = HttpClient::newHttpClient("http://localhost:8080");
    auto req = HttpRequest::newHttpRequest();
    req->setPath("/");
    client->sendRequest(req, [TEST_CTX](ReqResult res, const HttpResponsePtr& resp) {
        // There's nothing we can do if the request didn't reach the server
        // or the server generated garbage.
        REQUIRE(res == ReqResult::Ok);
        REQUIRE(resp != nullptr);

        CHECK(resp->getStatusCode() == k200OK);
        CHECK(resp->contentType() == CT_APPLICATION_JSON);
        CHECK(resp->body() == "{\"links\":[{\"href\":\"http://localhost:8080/template\",\"rel\":\"self\",\"type\":\"POST\"}]}");
    });
}

DROGON_TEST(TemplateRouteTest)
{
    auto client = HttpClient::newHttpClient("http://localhost:8080");
    auto req = HttpRequest::newHttpFormPostRequest();
    req->setMethod(Post);
    // req->;
    req->setPath("/template");
    client->sendRequest(req, [TEST_CTX](ReqResult res, const HttpResponsePtr& resp) {
        REQUIRE(res == ReqResult::Ok);
        REQUIRE(resp != nullptr);

        CHECK(resp->getStatusCode() == k200OK);
        CHECK(resp->body() == "{[\"error\"]} = \"\"");
    });
}

int main(int argc, char** argv) 
{

    std::promise<void> p1;
    std::future<void> f1 = p1.get_future();

    // Start the main loop on another thread
    std::thread thr([&]() {
        // Queues the promise to be fulfilled after starting the loop
        app().getLoop()->queueInLoop([&p1]() { p1.set_value(); });
        app().run();
    });

    // The future is only satisfied after the event loop started
    f1.get();
    int status = test::run(argc, argv);

    // Ask the event loop to shutdown and wait
    app().getLoop()->queueInLoop([]() { app().quit(); });
    thr.join();
    return status;
}
