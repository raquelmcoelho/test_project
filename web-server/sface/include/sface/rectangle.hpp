/**
 ******************************************************************************
 * @file    rectangle.hpp
 * @author  Support - support@visica.com.br
 * @version v0.0.0
 * @date    22 de abr de 2020
 * @brief   No comments
 ******************************************************************************
 * @attention
 *
 * <h2><center> COPYRIGHT &copy; 2021 Vísica AI Solutions     </center></h2>
 * <h2><center>            All Rights Reserved &reg;          </center></h2>
 *
 * Licensed under Vísica Software License Agreement, (the "License");
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 *        https://www.visica.com.br/license
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 ******************************************************************************
 */
#ifndef SFACE_RECTANGLE_HPP_
#define SFACE_RECTANGLE_HPP_

namespace sfacelib {

    /**
     * @brief Rectangle class is used to manipulate the information of an 
     *        object within an image
     */
    template <typename _Tp = long>
    class rectangle_basic {
    public:
        rectangle_basic() = default;
        rectangle_basic(_Tp left, _Tp top, _Tp right, _Tp bottom)
            : _x1(left)
            , _y1(top)
            , _x2(right)
            , _y2(bottom) {
        }

        virtual ~rectangle_basic() = default;

        inline bool is_null() const noexcept {
            return _x2 == _x1 - 1 && _y2 == _y1 - 1;
        }

        inline bool empty() const noexcept {
            return _x1 > _x2 || _y1 > _y2;
        }

        inline bool is_valid() const noexcept {
            return _x1 <= _x2 && _y1 <= _y2;
        }

        inline _Tp &left() {
            return _x1;
        }

        inline _Tp &top() {
            return _y1;
        }

        inline _Tp &right() {
            return _x2;
        }

        inline _Tp &bottom() {
            return _y2;
        }

        inline _Tp left() const noexcept {
            return _x1;
        }

        inline _Tp top() const noexcept {
            return _y1;
        }

        inline _Tp right() const noexcept {
            return _x2;
        }

        inline _Tp bottom() const noexcept {
            return _y2;
        }

        inline _Tp x() const noexcept {
            return _x1;
        }

        inline _Tp y() const noexcept {
            return _y1;
        }

        inline _Tp width() const noexcept {
            return _x2 - _x1 + 1;
        }

        inline _Tp height() const noexcept {
            return _y2 - _y1 + 1;
        }

        inline _Tp area() const noexcept {
            return width() * height();
        }

        inline bool operator<(const rectangle_basic &_rect) const noexcept {
            return area() < _rect.area();
        }

        inline bool operator<=(const rectangle_basic &_rect) const noexcept {
            return area() <= _rect.area();
        }

        inline bool operator>(const rectangle_basic &_rect) const noexcept {
            return area() > _rect.area();
        }

        inline bool operator>=(const rectangle_basic &_rect) const noexcept {
            return area() >= _rect.area();
        }

        template <typename _Vp>
        inline rectangle_basic<_Tp> &operator*=(_Vp value) {
            _x1 *= value;
            _y1 *= value;
            _x2 *= value;
            _y2 *= value;
        }

        template <typename _Vp>
        inline rectangle_basic<_Tp> &operator/=(_Vp value) {
            static_assert(value != value(), "value must be more than zero");
            _x1 /= value;
            _y1 /= value;
            _x2 /= value;
            _y2 /= value;
        }

        template <typename _Rect>
        _Rect extract_to(bool rotation = false) {
            return (rotation) ? _Rect(right(), bottom(), x() - right(), y() - bottom())
                              : _Rect(left(), top(), right(), bottom());
        }

    private:
        _Tp _x1 = 0;
        _Tp _y1 = 0;
        _Tp _x2 = -1;
        _Tp _y2 = -1;
    };

    using rectangle = rectangle_basic<>;
    using rectangle_f = rectangle_basic<float>;
} // namespace sfacelib

#endif /* SFACE_RECTANGLE_HPP_ */
