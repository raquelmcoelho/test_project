/**
 ******************************************************************************
 * @file    sface.hpp
 * @author  Support - support@visica.com.br
 * @version v0.0.1
 * @date    5 de jul de 2019
 * @brief   No comments
 ******************************************************************************
 * @attention
 *
 * <h2><center> COPYRIGHT &copy; 2021 Vísica AI Solutions     </center></h2>
 * <h2><center>            All Rights Reserved &reg;          </center></h2>
 *
 * Licensed under Vísica Software License Agreement, (the "License");
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 *        https://www.visica.com.br/license
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 ******************************************************************************
 */
#ifndef SFACE_DD_FACE_HPP_
#define SFACE_DD_FACE_HPP_

#include <functional>
#include <string>
#include <tuple>
#include <vector>

#include <sface/facesdetect.hpp>
#include <sface/image.hpp>
#include <sface/stypes.hpp>
#include <sface/usertemplate.hpp>

namespace sfacelib {
    /**
     * DDFace library implementation
     */
    class sface {
    public:
        /**
         * Default constructor, use this to create a single face recognition
         * class implementation.
         * A database must be added to this class before using
         */
        sface();

        /**
         * Use this to instanciate a face recognition with a database
         * @param DatabaseT : \see sqlite3database
         * @param database : A DatabaseT class instances
         */
        template <typename DatabaseT>
        sface(const DatabaseT &database);

        /**
         * Default destructor
         */
        virtual ~sface();

        /**
         * Add external database for match users from identified faces
         *
         * @param DatabaseT  : \see sqlite3database for an example
         * @param database : Database pointer for add to class recognition
         */
        template <typename DatabaseT>
        void add(const DatabaseT &database);

        /**
         * @brief Extract features of a face from an image with information
         *        about its position in the image.
         * 
         * @param image : Image that has a face to extract features
         * @param face : Face position
         * @return user_template_t user descriptor
         */
        user_template_t extract_features(const image &image, const rectangle &face);

        /**
         * @brief Extract features of a face from an image.
         * 
         * @param faces : Image and position data
         * @return users_templates_t user descriptor
         */
        users_templates_t extract_features(const faces_detect &faces);

        /**
         * @brief Get user descriptor for enroll user to databases. This method process 
         *        some rotations, mirrors and so one.
         * 
         * @param faces : Image and position data
         * @return users_templates_t 
         */
        users_templates_t enroll(const faces_detect &faces);

        /**
         * @brief Get user descriptor for enroll user to databases.
         * 
         * @param person_faces : Some images from a same user
         * @return user_template_t 
         */
        user_template_t enroll(const std::vector<faces_detect> &person_faces);

        /**
         * @brief Identify an user from databases
         * 
         * @param faces : User image data to search in databases
         * @return users_templates_t 
         */
        users_templates_t identify(const faces_detect &faces);

        /**
         * @brief Identify an user from databases
         * 
         * @param image : Image that has a face from an user
         * @param face : Face position on image
         * @return user_template_t 
         */
        user_template_t identify(const image &image, const rectangle &face);

        /**
         * @brief Verify an user from databases
         * 
         * @param id_number : ID in the database
         * @param face : Face data to search in databases
         * @return user_template_t : User verified content or empty
         */
        user_template_t verify(int id_number, const faces_detect &face);

        /**
         * @brief Verify an user from databases
         * 
         * @param name : user name
         * @param face : Face data to search in databases
         * @return user_template_t : User verified content or empty
         */
        user_template_t verify(const std::string &name, const faces_detect &face);

        /**
         * @brief Verify an user from databases
         * 
         * @param user : User descriptor for match
         * @param face : Face data to search in databases
         * @return true : User verified, false - User not verified
         */
        bool verify(const user_template_t &user, const faces_detect &face);

        /**
         * @brief Get the user from key method.
         * 
         * @param key : integer value
         * @return user_template_t content or empty 
         */
        user_template_t get_user_from_key(int key) const;

        /**
         * @brief Get the user from key method.
         * 
         * @param name : string value
         * @return user_template_t content or empty 
         */
        user_template_t get_user_from_name(const std::string &name) const;

        /**
         * Clear all databases
         */
        void clear();

        /**
         * Enable logger print
         */
        void enable_logger();

        /**
         * Disable logger print
         */
        void disable_logger();

        /**
         * Set the threshold value for identification
         * \param value : The value must be between 0.0 to 1.0
         */
        void set_threshold(double value);

        /**
         * Get threshold used to recognition model
         * \return threshold value
         */
        double threshold() const;

        /**
         * Get total elements enrolled to databases
         * \return Total elements
         */
        std::size_t size() const;

        /**
         * Get total users that may enrolled in database
         * \return Max users to enrolled
         */
        std::size_t total() const;

        /**
         * Get total database elements added before
         */
        std::size_t databases_count() const;

    private:
        //! Face private implementation
        SF_DECLARE_PRIVATE(sface);

        //! removing copies for this class
        sface(const sface &) = delete;
        sface &operator=(const sface &) = delete;
    };
} // namespace sfacelib

#endif /* SFACE_DD_FACE_HPP_ */
