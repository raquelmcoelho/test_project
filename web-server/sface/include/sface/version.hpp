
/**
 * \mainpage Smart Face Recognition Library - Vísica AI Solutions.
 *
 * SFACE is a library used for facial recognition of private rights.
 *
 * @attention
 *
 * <h2><center>&copy; COPYRIGHT 2021 2021 Vísica AI Solutions </center></h2>
 * <h2><center>            All Rights Reserved &reg;          </center></h2>
 *
 * Licensed under Vísica AI Solutions Software License Agreement, (the "License");
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 *        https://www.visica.com.br/license
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 ******************************************************************************
 */

#ifndef SFACE_VERSION_HPP_
#define SFACE_VERSION_HPP_

/*
 * SFACE_VERSION is (major << 24) + (minor << 16) + releases.
 */
#define SFACE_VERSION_INT      SFACE_VERSION_CHECK(SFACE_MAJOR_VERSION, SFACE_MINOR_VERSION, SFACE_REVISION_VERSION)

/*
 can be used like #if (SFACE_VERSION >= SFACE_VERSION_CHECK(4, 4, 0))
 */
#define SFACE_VERSION_CHECK(major, minor, releases) ((major<<16)|(minor<<8)|(releases))

/** X.x.xx: Major version of the project */
#define SFACE_MAJOR_VERSION                1
/** x.X.xx: Minor version of the project */
#define SFACE_MINOR_VERSION                1
/** x.x.XX: Revision of the project */
#define SFACE_REVISION_VERSION             1

/** Provides the version of the project example 1.0.1048 */
#define SFACE_VERSION                      (SFACE_MAJOR_VERSION << 24 | \
                                            SFACE_MINOR_VERSION << 16 | \
                                            SFACE_REVISION_VERSION)

#define SFACE_VERSION_CONV_STR(X)           #X
#define SFACE_VERSION_STRX(A, B, C)         SFACE_VERSION_CONV_STR(A) "." SFACE_VERSION_CONV_STR(B) "." SFACE_VERSION_CONV_STR(C)
#define SFACE_VERSION_STRING()              SFACE_VERSION_STRX(SFACE_MAJOR_VERSION,SFACE_MINOR_VERSION,SFACE_REVISION_VERSION)
#define SFACE_VERSION_STR                   SFACE_VERSION_STRING()

#define SFACE_APPLICATION_NAME              "sface"
#define SFACE_ORGANIZATION                  "visica"
#define SFACE_DOMAIN                        "br.com.visica.libsface"

#endif /* SFACE_VERSION_HPP_ */
