/**
 ******************************************************************************
 * @file    sfaceerrors.hpp
 * @author  Support - support@visica.com.br
 * @version v0.0.1
 * @date    2021, November 17
 * @brief   This module throw exception with error code list
 *          \ref sface_error_codes for more information
 *
 ******************************************************************************
 * @attention
 *
 * <h2><center> COPYRIGHT &copy; 2021 Vísica AI Solutions     </center></h2>
 * <h2><center>            All Rights Reserved &reg;          </center></h2>
 *
 * Licensed under Vísica Software License Agreement, (the "License");
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 *        https://www.visica.com.br/license
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 ******************************************************************************
 */
#ifndef SFACEERRORS_HPP_
#define SFACEERRORS_HPP_

#include <exception>
#include <string>

//! This is used for sface library to exception throws
#ifdef __GNUC__
#define sface_throw(code, ...) throw sfacelib::sface_exception(code, __FILE__, __LINE__, __FUNCTION__, __VA_ARGS__)
#else
#define sface_throw(code, ...) throw sfacelib::sface_exception(code, __FILE__, __LINE__, __FUNCTION__, __VA_ARGS__)
#endif

namespace sfacelib {
    enum class sface_error_codes {
        NO_ERROR,
        UNKNOWN = -1,

        //! Globals errors
        INVALID_PARAMETER = 10, //!< Invalid parameter was used in any method
        INVALID_THRESHOLD,      //!< The threshold value must between 0.0 and 1.0

        //! License errors
        INVALID_LICENSE = 100, //!< Invalid license file or not exists
        LICENSE_EXPIRED,       //!< License date expired
        MAX_USERS_INVALID,     //!< The maximum number of users has been reached

        //! Database errors
        DATABASE_IS_NULL = 200,
    };

    class sface_exception : public std::exception {
        std::string __result;
        sface_error_codes __code;
        
    public:
        sface_exception(sface_error_codes code,
                         const std::string &file,
                         unsigned int line,
                         const std::string &function,
                         const std::string &message = std::string()) noexcept
            : __result("sface exception: ")
            , __code(code) {
            static_assert(__cplusplus >= 201103L, "you must enable c++11 or greater");
            if (!message.empty())
                __result += message + " on ";
            __result += function;
            __result += ", line ";
            __result += std::to_string(line);
            __result += ", ";
            __result += file;
            __result += ": error code[";
            __result += std::to_string(static_cast<int>(code));
            __result += "]";
        }

        virtual ~sface_exception() noexcept = default;
        sface_exception(const sface_exception &) = default;
        sface_exception &operator=(const sface_exception &) = default;
        sface_exception(sface_exception &&) = default;
        sface_exception &operator=(sface_exception &&) = default;

        virtual const char *what() const noexcept override final {
            return __result.c_str();
        }

        sface_error_codes code() const noexcept {
            return __code;
        }
    };

    struct sface_error_helper {
        static void error(std::exception &e) {
            sface_throw(sface_error_codes::INVALID_LICENSE, e.what());
        }
    };
} // namespace sfacelib

#endif /* SFACEERRORS_HPP_= */