/**
 ******************************************************************************
 * @file    configurations.hpp
 * @author  Support - support@visica.com.br
 * @version v0.0.0
 * @date    30 de abr de 2020
 * @brief   No comments
 ******************************************************************************
 * @attention
 *
 * <h2><center> COPYRIGHT &copy; 2021 Vísica AI Solutions     </center></h2>
 * <h2><center>            All Rights Reserved &reg;          </center></h2>
 *
 * Licensed under Vísica Software License Agreement, (the "License");
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 *        https://www.visica.com.br/license
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 ******************************************************************************
 */
#ifndef SFACE_CONFIGURATIONS_HPP_
#define SFACE_CONFIGURATIONS_HPP_

#include <string>

namespace sfacelib {
    namespace configurations {
        struct sface_config {
            unsigned int jitters = 10; // default 100
            unsigned int min_face = 80;
            double threshold = 0.4; // default 0.4
        };

        /**
         * Return the path where application data location will be used
         * \return absolute path
         */
        std::string get_application_data_location();

    } /* namespace configurations */
} /* namespace sfacelib */

#endif /* SFACE_CONFIGURATIONS_HPP_ */
