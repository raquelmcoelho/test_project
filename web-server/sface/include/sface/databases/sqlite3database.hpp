/**
 ******************************************************************************
 * @file    sqlite3database.hpp
 * @author  Support - support@visica.com.br
 * @version v0.0.0
 * @date    29 de abr de 2020
 * @brief   No comments
 ******************************************************************************
 * @attention
 *
 * <h2><center> COPYRIGHT &copy; 2021 Vísica AI Solutions     </center></h2>
 * <h2><center>            All Rights Reserved &reg;          </center></h2>
 *
 * Licensed under Vísica Software License Agreement, (the "License");
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 *        https://www.visica.com.br/license
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 ******************************************************************************
 */
#ifndef SFACE_DATABASES_SQLITE3DATABASE_HPP_
#define SFACE_DATABASES_SQLITE3DATABASE_HPP_

#include <sface/stypes.hpp>
#include <sface/usertemplate.hpp>

#include <set>

namespace sfacelib {
    namespace databases {
        /**
         * @brief SQLITE3 database class.
         *
         * Used to create, remove, insert and select users.
         */
        class sqlite3_database {
        public:
            /**
             * @brief Construct a new sqlite3 database object
             */
            sqlite3_database();

            /**
             * @brief Construct a new sqlite3 database object
             *
             * @param location : Location where the database file must be created
             */
            sqlite3_database(const std::string &location);

            /**
             * @brief Destroy the sqlite3 database object
             */
            virtual ~sqlite3_database();

            /**
             * @brief Save an user template to databases
             *
             * @param user : User descriptor data
             */
            void add(user_template_t &&user);

            /**
             * @brief Save an user template to databases
             *
             * @param user : User descriptor data
             */
            void add(const user_template_t &user);

            /**
             * @brief Get the user method.
             * @tparam T : type of finder
             * @param __v : type content
             * @return user_template_t content or empty
             */
            template <typename T>
            user_template_t get_user(const T &__v);

            /**
             * @brief Load users from database
             *
             * @param desc : To alignment
             * @return std::set<user_template_t>
             */
            std::set<user_template_t> load(bool desc = false);

            /**
             * @brief Get the last database record
             *
             * @return user_template_t : User descriptor data
             */
            user_template_t last();

            /**
             * @brief Remove user from SQLITE3 database
             *
             * @param user : User template data to remove
             */
            void remove(user_template_t &user);

            /**
             * @brief Remove user from SQLITE3 database
             *
             * @param user : User template data to remove
             */
            void remove(user_template_t &&user);

            /**
             * @brief Remove all users from SQLITE3 database
             */
            void delete_all();

        private:
            //! Private class
            SF_DECLARE_PRIVATE(sqlite3_database);
        };

    } /* namespace databases */
} /* namespace sfacelib */

#endif /* SFACE_DATABASES_SQLITE3DATABASE_HPP_ */
