/**
 ******************************************************************************
 * @file    database.hpp
 * @author  Support - support@visica.com.br
 * @version v1.0.2
 * @date    27 de abr de 2020
 * @brief   No comments
 ******************************************************************************
 * @attention
 *
 * <h2><center> COPYRIGHT &copy; 2021 Vísica AI Solutions     </center></h2>
 * <h2><center>            All Rights Reserved &reg;          </center></h2>
 *
 * Licensed under Vísica Software License Agreement, (the "License");
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 *        https://www.visica.com.br/license
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 ******************************************************************************
 */
#ifndef SFACE_DATABASE_HPP_
#define SFACE_DATABASE_HPP_

#include <algorithm>
#include <memory>
#include <set>
#include <type_traits>
#include <vector>

#include <sface/databases/sqlite3database.hpp>
#include <sface/usertemplate.hpp>

namespace sfacelib {
    namespace databases {

        /**
         * Database class definition
         */
        template <typename _Tdriver>
        class database : public std::set<user_template_t> {
            //! Copies is not allowed
            database(const database &) = delete;
            database &operator=(const database &) = delete;

            //! Database driver object
            _Tdriver __db_driver;

        public:
            using driver_type = _Tdriver;
            using database_type = database<_Tdriver>;
            using pointer = std::shared_ptr<database_type>;
            using const_pointer = const pointer;
            using reference = database_type &;
            using const_reference = const database_type &;

            /**
             * Constructor default for this class.
             * The default database location will be load here.
             */
            database() = default;

            /**
             * Use this constructor for create a database instance and tell
             * the databases file location
             *
             * @param location  : Database file location
             */
            database(const std::string &location)
                : __db_driver(location) {
            }

            /**
             * Destructor default
             */
            virtual ~database() = default;

            /**
             * @brief Method to create this class types
             *
             * @tparam Args : Types of the arguments
             * @param args : Arguments from types
             * @return pointer : pointer created to this class manipulates
             */
            template <typename... Args>
            static pointer create(Args... args) {
                return std::make_shared<database_type>(std::forward<Args>(args)...);
            }

            /**
             * Add user to database
             * @param __u : User data template, \see user_template_t or users_templates_t
             */
            template <typename _Tp>
            void add(_Tp &__u) {
                static_assert(std::is_same_v<_Tp, user_template_t> or std::is_same_v<_Tp, users_templates_t>,
                              "type must be user_template_t or users_templates_t");
                if (__u.empty())
                    std::invalid_argument("user template must not be empty");

                if constexpr (std::is_same_v<_Tp, user_template_t>) {
                    if (not __u.is_ok())
                        std::invalid_argument("user name must not be empty");
                    __db_driver.add(static_cast<_Tp &&>(__u));
                    insert(__u);
                } else {
                    for (const auto &u : __u) {
                        if (not u.is_ok())
                            std::invalid_argument("user name must not be empty");
                        __db_driver.add(static_cast<_Tp &&>(u));
                        insert(u);
                    }
                }
            }

            /**
             * Add user to database
             * @param __u : User data template, \see user_template_t or users_templates_t
             */
            template <typename _Tp>
            void add(_Tp &&__u) {
                static_assert(!std::is_lvalue_reference<_Tp>::value,
                              "template argument"
                              " substituting _Tp is an lvalue reference type");
                static_assert(std::is_same_v<_Tp, user_template_t> or std::is_same_v<_Tp, users_templates_t>,
                              "type must be user_template_t or users_templates_t");
                if (__u.empty())
                    std::invalid_argument("user template must not be empty");

                if constexpr (std::is_same_v<_Tp, user_template_t>) {
                    if (not __u.is_ok())
                        std::invalid_argument("user name must not be empty");
                    __db_driver.add(__u);
                    insert(__u);
                } else {
                    for (const auto &u : __u) {
                        if (not u.is_ok())
                            std::invalid_argument("user name must not be empty");
                        __db_driver.add(u);
                        insert(u);
                    }
                }
            }

            /**
             * @brief Remove user from databases
             *
             * @tparam _Tp : user_template_t or user_templates_t
             * @param __u : User template data
             */
            template <typename _Tp>
            void remove(_Tp &__u) {
                if (__u.empty())
                    std::invalid_argument("user template must not be empty");
                if constexpr (std::is_same_v<_Tp, user_template_t>) {
                    if (not __u.is_ok())
                        std::invalid_argument("user name must not be empty");
                    __db_driver.remove(__u);
                    insert(__u);
                } else {
                    for (const auto &u : __u) {
                        if (not u.is_ok())
                            std::invalid_argument("user name must not be empty");
                        __db_driver.remove(u);
                        insert(u);
                    }
                }
            }

            /**
             * @brief Remove user from databases
             *
             * @tparam _Tp : user_template_t or user_templates_t
             * @param __u : User template data
             */
            template <typename _Tp>
            void remove(_Tp &&__u) {
                if (__u.empty())
                    std::invalid_argument("user template must not be empty");
                if constexpr (std::is_same_v<_Tp, user_template_t>) {
                    if (not __u.is_ok())
                        std::invalid_argument("user name must not be empty");
                    __db_driver.remove(__u);
                    insert(__u);
                } else {
                    for (const auto &u : __u) {
                        if (not u.is_ok())
                            std::invalid_argument("user name must not be empty");
                        __db_driver.remove(u);
                        insert(u);
                    }
                }
            }

            /**
             * @brief Remove all users from databases
             */
            void remove_all() {
                __db_driver.delete_all();
            }

            /**
             * Load the user templates from database
             * \return User counts
             */
            std::size_t load(bool desc = false) {
                auto users = __db_driver.load(desc);
                if (not users.empty()) {
                    clear();
                    std::swap(users, *this);
                }
                return size();
            }

            /**
             * @brief Get the user method
             * @tparam T : type of finder
             * @param __v : type content
             * @return user_template_t content or empty
             */
            template <typename T>
            user_template_t get_user(const T &__v) {
                return __db_driver.get_user(__v);
            }

            /**
             * Check user exists by name
             * \param name  : User name to find
             * \return true on success found
             */
            bool exists(const std::string &name) {
                return std::find_if(begin(), end(), [&name](const auto &user) { return user.name() == name; }) != end();
            }
        };

        /**
         * \note For conveniences, use this types bellow
         */
        using database_sqlite3 = database<sqlite3_database>;

    } /* namespace databases */
} // namespace sfacelib

#endif /* SFACE_DATABASE_HPP_ */
