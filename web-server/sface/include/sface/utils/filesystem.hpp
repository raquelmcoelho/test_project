/**
 ******************************************************************************
 * @file    filesystem.hpp
 * @author  Support - support@visica.com.br
 * @version v0.0.0
 * @date    5 de jul de 2019
 * @brief   No comments
 ******************************************************************************
 * @attention
 *
 * <h2><center> COPYRIGHT &copy; 2021 Vísica AI Solutions     </center></h2>
 * <h2><center>            All Rights Reserved &reg;          </center></h2>
 *
 * Licensed under Vísica Software License Agreement, (the "License");
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 *        https://www.visica.com.br/license
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 ******************************************************************************
 */

#ifndef SFACE_UTILS_FILESYSTEM_HPP_
#define SFACE_UTILS_FILESYSTEM_HPP_

#include <functional>
#include <map>
#include <regex>
#include <set>
#include <string>

namespace sfacelib {
    /**
     * Filesystem Utils
     */
    namespace fs {
        using file_list_t = std::map<std::string, std::multiset<std::string>>;

        /**
         * List all files from path
         * \param path  : Path to list files
         * \param cb    : Callback function to use as possible, filename as parameter
         * \return map between path and files
         *
         * \code
         * #include <iostream>
         * #include <sface/utils/filesystem.hhp>
         * int main() {
         *     std::string path = "/path/images/faces/MEDS";
         *     auto lst = list_files(path, [](const std::string &file) {
         *         std::cout << file << std::endl;
         *     });
         *     return 0;
         *}
         *}\endcode
         */
        file_list_t list_files(const std::string &path,
                               const std::function<void(const std::string &filename)> &cb = nullptr);

        /**
         * List all files from path
         * \param path  : Path to list files
         * \param extension : Filter extension of the files
         * \param cb    : Callback function to use as possible, filename as parameter
         * \return map between path and files
         *
         * \code
         * #include <iostream>
         * #include <sface/utils/filesystem.hhp>
         * int main() {
         *     std::string path = "/path/images/faces/MEDS";
         *     auto lst = list_files(path, ".jpg", [](const std::string &file) {
         *         std::cout << file << std::endl;
         *     });
         *     return 0;
         *}
         *}\endcode
         */
        file_list_t list_files(const std::string &path,
                               const std::string &extension,
                               const std::function<void(const std::string &filename)> &cb = nullptr);

        /**
         * List all files from path
         * \param path  : Path to list files
         * \param file_matcher  : Filter files from regex pattern
         * \param cb    : Callback function to use as possible, filename as parameter
         * \return map between path and files
         *
         * \code
         * #include <iostream>
         * #include <sface/utils/filesystem.hhp>
         * int main() {
         *     static const std::regex image_files( "(.*).(?:jpg|png)" ) ;
         *     std::string path = "/path/images/faces/MEDS";
         *     auto lst = list_files(path, image_files, [](const std::string &file) {
         *         std::cout << file << std::endl;
         *     });
         *     return 0;
         *}
         *}\endcode
         */
        file_list_t list_files(const std::string &path,
                               const std::regex &file_matcher,
                               const std::function<void(const std::string &filename)> &cb = nullptr);

        /**
         * Check if file exists in the filesystem
         * \param name  : absolute path of the file
         * \return
         *      @arg true   : File exists
         *      @arg false  : File not exists
         */
        bool exists(const std::string &name);

        /**
         * Load the directory name
         * \param path  : absolute path to directory
         * \return \example /home/name/images
         */
        std::string dirname(const std::string &path);

        /**
         * Show filename from absolute path
         * \param path  : Path to file --> \example /home/name/files/system.log
         * \return
         *      result base of the name, \example 'system'
         */
        const std::string _basename(const std::string &path);

        /**
         * Show filename from absolute path
         * \param path  : Path to file --> \example /home/name/files/system.log
         * \return
         *      result base of the name, \example 'system.log'
         */
        const std::string filename(const std::string &path);

        /**
         * Show extension from file source
         * \param path  : absolute file \example /home/name/files/system.log
         * \return ".log"
         */
        const std::string extension(const std::string &path);

        /**
         * Create directories and sub-directories
         * \param path  : absolute directory
         * \return
         *      @arg true   : Directories were created successfully
         *      @arg false  : Directories have not been created
         */
        bool create_directories(const std::string &path);
    } // namespace fs

} /* namespace sfacelib */

#endif /* SFACE_UTILS_FILESYSTEM_HPP_ */
