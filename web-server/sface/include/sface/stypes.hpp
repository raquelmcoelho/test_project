/**
 ******************************************************************************
 * @file    stypes.hpp
 * @author  Support - support@visica.com.br
 * @version v0.0.0
 * @date    29 de jul de 2021
 * @brief   No comments
 ******************************************************************************
 * @attention
 *
 * <h2><center> COPYRIGHT &copy; 2021 Vísica AI Solutions     </center></h2>
 * <h2><center>            All Rights Reserved &reg;          </center></h2>
 *
 * Licensed under Vísica Software License Agreement, (the "License");
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 *        https://www.visica.com.br/license
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 ******************************************************************************
 */
#ifndef SFACE_STYPES_HPP_
#define SFACE_STYPES_HPP_

#include <memory> // for std::unique_ptr

#define SF_DECLARE_PRIVATE(clazz) \
    class clazz##_private; \
    friend class clazz##_private; \
    std::unique_ptr<clazz##_private> d_ptr; 

#endif /* SFACE_STYPES_HPP_ */
