/**
 ******************************************************************************
 * @file    usertemplate.h
 * @author  Support - support@visica.com.br
 * @version v0.0.0
 * @date    5 de jul de 2019
 * @brief   No comments
 ******************************************************************************
 * @attention
 *
 * <h2><center> COPYRIGHT &copy; 2021 Vísica AI Solutions     </center></h2>
 * <h2><center>            All Rights Reserved &reg;          </center></h2>
 *
 * Licensed under Vísica Software License Agreement, (the "License");
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 *        https://www.visica.com.br/license
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 ******************************************************************************
 */
#ifndef SFACE_TEMPLATES_USERTEMPLATE_HPP_
#define SFACE_TEMPLATES_USERTEMPLATE_HPP_

#include <sface/stypes.hpp>
#include <string>
#include <vector>

namespace sfacelib {

    /**
     * User Template class implementation
     */
    class user_template : public std::vector<double> {
    public:
        /**
         * Default constructor
         */
        user_template();

        /**
         * @brief Copy Construct
         * @param user_template &other
         */
        user_template(const user_template &);

        /**
         * @brief Move Construct
         * @param user_template &other
         */
        user_template(user_template &&) noexcept;

        /**
         * @brief Copy user template class
         * @return user_template& other
         */
        user_template &operator=(const user_template &);

        /**
         * Default destructor
         */
        virtual ~user_template();

        /**
         * Set a new key to user class
         * \param value : New value to set
         */
        void set_key(const int &value);

        /**
         * User identification value
         * \return  : \ref user_ype value
         */
        int key() const;

        /**
         * Set name to this template
         * \param name  : New name value
         */
        void set_name(const std::string &name);

        /**
         * Get name from this template
         * \return Name
         */
        std::string name() const;

        /**
         * Set distance on match user from database
         * @param distance  : Value from 0.0 to 1.0
         */
        void set_distance(double distance);

        /**
         * Get distance on match
         * @return Distance value from 0.0 to 1.0
         */
        double distance() const;

        /**
         * Compares for order user templates
         * \param other : other user template instance
         */
        bool operator<(const user_template &other) const;

        /**
         * Compares for order user templates
         * \param other : other user template instance
         */
        bool operator<=(const user_template &other) const;

        /**
         * Compares for order user templates
         * \param other : other user template instance
         */
        bool operator>(const user_template &other) const;

        /**
         * Compares for order user templates
         * \param other : other user template instance
         */
        bool operator>=(const user_template &other) const;

        /**
         * User template is valid
         */
        bool is_ok() const;

        /**
         * @brief Get the descriptor in hexadecimal string
         * 
         * Implemented types: 
         * T = std::string (Hexadecimal String)
         * T = std::vector<char> (Array of chars)
         * @return std::string, descriptor content
         */
        template <typename T>
        T get_descriptor() const;

        /**
         * @brief Set the descriptor method.
         * 
         * Implemented types: 
         * T = std::string (Hexadecimal String)
         * T = std::vector<char> (Array of chars)
         * 
         * @tparam T 
         * @param _descriptor 
         */
        template <typename T>
        void set_descriptor(const T &_descriptor);

    private:
        SF_DECLARE_PRIVATE(user_template); //!< Private user template class
    };

    //! Default user template type
    using user_template_t = user_template;

    //! Users templates
    using users_templates_t = std::vector<user_template_t>;

} // namespace sfacelib

#endif /* SFACE_TEMPLATES_USERTEMPLATE_HPP_ */
