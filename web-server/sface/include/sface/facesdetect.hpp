/**
 ******************************************************************************
 * @file    facesdetect.hpp
 * @author  Support - support@visica.com.br
 * @version v0.0.0
 * @date    22 de abr de 2020
 * @brief   No comments
 ******************************************************************************
 * @attention
 *
 * <h2><center> COPYRIGHT &copy; 2021 Vísica AI Solutions     </center></h2>
 * <h2><center>            All Rights Reserved &reg;          </center></h2>
 *
 * Licensed under Vísica Software License Agreement, (the "License");
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 *        https://www.visica.com.br/license
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 ******************************************************************************
 */
#ifndef SFACE_FACESDETECT_HPP_
#define SFACE_FACESDETECT_HPP_

#include <sface/image.hpp>
#include <sface/rectangle.hpp>
#include <sface/stypes.hpp>

#include <memory>
#include <vector>

namespace sfacelib {

    /**
     * Face Filter Class Implementation
     */
    class faces_detect : public std::vector<sfacelib::rectangle> {
    public:
        /**
         * Constructor default
         */
        faces_detect();

        /**
         * Destructor default
         */
        virtual ~faces_detect();

        /**
         * Process an image to detect faces
         * @param image : Image that will be processed
         * @param autoresize    : Auto resize image for performance
         * @param num_times     : Up or Down samples times
         */
        faces_detect(const image &image, bool autoresize = true, const int num_times = 0);

        /**
         * @brief Detect one or more faces from an image.
         * 
         * @param image : Image class object
         * @param liveness : Check if faces has liveness
         * @return faces_detect& 
         */
        faces_detect &operator()(const image &image, bool liveness = false);

        /**
         * @brief Get the image that the faces ware found
         * 
         * @return sfacelib::image& 
         */
        sfacelib::image &img();

        /**
         * @brief Get the image that the faces ware found (const)
         * 
         * @return const sfacelib::image& 
         */
        const sfacelib::image &img() const;

        /**
         * @brief Check if image detect has liveness.
         * @note This method requires a license feature enabled
         * 
         * @return true - Has a liveness image
         * @return false - Has no liveness image
         */
        bool has_liveness() const;

    private:
        //! Private Class Declaration
        SF_DECLARE_PRIVATE(faces_detect)

        //! Image that the face was detected
        sfacelib::image _img;
    };

} // namespace sfacelib

#endif /* SFACE_FACESDETECT_HPP_ */
