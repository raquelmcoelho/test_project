/**
 ******************************************************************************
 * @file    image.hpp
 * @author  Support - support@visica.com.br
 * @version v0.0.1
 * @date    22 de abr de 2020
 * @brief   No comments
 ******************************************************************************
 * @attention
 *
 * <h2><center> COPYRIGHT &copy; 2021 Vísica AI Solutions     </center></h2>
 * <h2><center>            All Rights Reserved &reg;          </center></h2>
 *
 * Licensed under Vísica Software License Agreement, (the "License");
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 *        https://www.visica.com.br/license
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 ******************************************************************************
 */
#ifndef SFACE_IMAGE_HPP_
#define SFACE_IMAGE_HPP_

#include <memory>
#include <string>

#include <sface/stypes.hpp>


namespace sfacelib {
    /**
     * Image class
     */
    class image {
    public:
        /**
         * \brief Image formats
         */
        enum class formats {
            invalid,
            rgb32,
            argb32,
            rgb16,
            rgb888,
            rgba8888,
            grayscale8,
            grayscale16,
            bgr888,
            abgr32,
            bgra32,
            nformats
        };

        /**
         * Constructor empty image for this class
         */
        image() noexcept;

        /**
         * Create an image empty, with format and sizes
         * \param width     : Width size value
         * \param height    : Height size value
         * \param format    : \ref enum class formats
         */
        image(int width, int height, formats format);

        /**
         * Create an image and put the data to it
         * \param width     : Width size value
         * \param height    : Height size value
         * \param data      : data in pixels
         * \param format    :  \ref enum class formats
         */
        image(int width, int height, char *data, formats format);

        /**
         * Create an image and put the data to it
         * \param width     : Width size value
         * \param height    : Height size value
         * \param data      : data in pixels, read only
         * \param format    :  \ref enum class formats
         */
        image(int width, int height, const char *data, formats format);

        /**
         * Create an image and put the data to it
         * \param width     : Width size value
         * \param height    : Height size value
         * \param data      : data in pixels
         * \param bytes_per_line : Value of the bytes per line, \example width * channels
         * \param format    :  \ref enum class formats
         */
        image(int width, int height, char *data, int bytes_per_line, formats format);

        /**
         * Create an image and put the data to it
         * \param width     : Width size value
         * \param height    : Height size value
         * \param data      : data in pixels, read only
         * \param bytes_per_line : Value of the bytes per line, \example width * channels
         * \param format    :  \ref enum class formats
         */
        image(int width, int height, const char *data, int bytes_per_line, formats format);

        /**
         * Default destructor for this class
         */
        virtual ~image();

        /**
         * Move constructor was implemented
         * \param other : Other object to move
         */
        image(image &&other) noexcept;

        /**
         * Copy constructor
         * \param other  : other image object to copy to this
         */
        image(const image &other);

        /**
         * Copy operator implementation
         * \param other : other image object to copy to this
         * \return *this
         */
        image &operator=(const image &other);

        /**
         * Create an image class from file loaded
         * \param file  : absolute path to file
         */
        image(const std::string &file);

        /**
         *  Image width value
         */
        std::size_t width() const;

        /**
         * Image height value
         */
        std::size_t height() const;

        /**
         * Check if sizes of this image is empty
         */
        bool empty() const;

        /**
         * Pixels data access
         * \return first pixel to image
         */
        char *pixels();

        /**
         * Pixels data access
         * \return first pixel to image
         */
        const char *pixels() const;

        /**
         * Bytes per line value
         * \return width * depth (channels)
         */
        std::size_t bytes_per_line() const;

        /**
         * Width and Height
         * \return width * height
         */
        std::size_t size() const;

        /**
         * Format of this image
         * \return \ref formats
         */
        formats format() const;

        /**
         * Convert o another format
         * \param format    : New format to convert
         * \return \ref image new formats
         */
        image convert(formats format) const;

        /**
         * Create an sfacelib::image instance from image file
         * \param file  : absolute path to file \example /home/name/images/image.[png|jpg]
         * \param img   : sfacelib::image reference
         */
        static void create(const std::string &file, image &img);

        /**
         * Create an sfacelib::image instance from image file
         * \param file  : absolute path to file \example /home/name/images/image.[png|jpg]
         * \return sfacelib::image instance
         */
        static image create(const std::string &file);

        /**
         * Load an image to this class
         * \param file  : absolute path to file \example /home/name/images/image.[png|jpg]
         */
        void load(const std::string &path);

        /**
         * Save an image file from this class
         * \param path  : absolute path to save the file \example /home/name/images/out.[png|jpg]
         */
        void save(const std::string &path) const;

        /**
         * @warning Not implemented
         */
        // image resize(std::size_t width, std::size_t height, int b) const;

        /**
         * This method is called from private data
         * \warning do not use this method
         */
        template <typename T>
        T &get();

        /**
         * This method is called from private data
         * \warning do not use this method
         */
        template <typename T>
        const T &get() const;

    private:
        //! Private Class Declaration
        SF_DECLARE_PRIVATE(image);
    };

} // namespace sfacelib

#endif /* SFACE_IMAGE_HPP_ */
