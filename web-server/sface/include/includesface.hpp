#ifndef INCLUDESFACE_HPP_
#define INCLUDESFACE_HPP_

#include <sface/configurations.hpp>
#include <sface/facesdetect.hpp>
#include <sface/image.hpp>
#include <sface/rectangle.hpp>
#include <sface/sface.hpp>
#include <sface/sfaceerrors.hpp>
#include <sface/stypes.hpp>
#include <sface/usertemplate.hpp>
#include <sface/version.hpp>

#endif
