# SFACE - Face Recognition Library
## Releases


### 1.1.0 - 2021-04-20 11:40
1. User template with AES256 encryption
2. Get user template method has been implemented
3. Encrypted user template in the database
4. A verification method was implemented

```How to use
	sfacelib::sface sf(db);
	auto user1 = sf.get_user_from_name("person 1"); // user_template_t
	auto user2 = sf.get_user_from_key(key); // user_template_t
	
        sfacelib::image img("some-image.jpg"); // image must not have sizes more than 640x480
        sfacelib::faces_detect faces(img, true);
        assert(not faces.empty());

        bool result = sf.verify(user, faces);
        std::cout << "verify result: " << result << std::endl;
```

### 1.1.1 - 2021-04-20 14:40
1. license file location bug fixed

