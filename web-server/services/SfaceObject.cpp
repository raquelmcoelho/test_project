#include <SfaceObject.h>
#include <includesface.hpp>
#include <iostream>

// can find this information at "config.json"
#define UPLOAD_DIR "uploads/"

using namespace std;

SfaceObject::SfaceObject(HttpFile *fileObject) {
    this->fileObject = fileObject;
    this->fileObject->save();
    this->filePath = UPLOAD_DIR + fileObject->getFileName();
    this->readBinaryToSetProperties();
}

SfaceObject::~SfaceObject() {
    remove(filePath.c_str());
}

void SfaceObject::readBinaryToSetProperties() {
    FILE *f=fopen(this->filePath.c_str(),"rb"); 
    if (f==0) return;

    fseek(f,0,SEEK_END); 
    long len=ftell(f); 
    fseek(f,0,SEEK_SET); 

    if (len<24) {
        fclose(f); 
        return;
    }

  // Strategy:
  // reading PNG dimensions requires the first 24 bytes of the file
  // reading JPEG dimensions requires scanning through jpeg chunks
  // In all formats, the file is at least 24 bytes big, so we'll read that always
    unsigned char buf[24]; fread(buf,1,24,f);

  // For JPEGs, we need to read the first 12 bytes of each chunk.
  // We'll read those 12 bytes at buf+2...buf+14, i.e. overwriting the existing buf.
    if (buf[0]==0xFF 
     && buf[1]==0xD8 
     && buf[2]==0xFF 
     && buf[3]==0xE0 
     && buf[6]=='J' 
     && buf[7]=='F' 
     && buf[8]=='I' 
     && buf[9]=='F') {
        long pos=2;

        while (buf[2]==0xFF) {
            if (buf[3]==0xC0 
             || buf[3]==0xC1 
             || buf[3]==0xC2 
             || buf[3]==0xC3 
             || buf[3]==0xC9 
             || buf[3]==0xCA 
             || buf[3]==0xCB) 
                break;

            pos += 2+(buf[4]<<8)+buf[5];
            if (pos+12>len) break;
            fseek(f,pos,SEEK_SET); 
            fread(buf+2,1,12,f);   
        }
    }


    fclose(f);

  // JPEG: (first two bytes of buf are first two bytes of the jpeg file; rest of buf is the DCT frame
    if (buf[0]==0xFF 
     && buf[1]==0xD8 
     && buf[2]==0xFF) {   
        this->extension = FileExtension::jpg;
        this->width = (buf[9]<<8) + buf[10]; 
        this->height = (buf[7]<<8) + buf[8]; 
        return;
    }

  // PNG: the first frame is by definition an IHDR frame, which gives dimensions
    if ( buf[0]==0x89 
      && buf[1]=='P' 
      && buf[2]=='N' 
      && buf[3]=='G' 
      && buf[4]==0x0D 
      && buf[5]==0x0A 
      && buf[6]==0x1A 
      && buf[7]==0x0A 
      && buf[12]=='I' 
      && buf[13]=='H' 
      && buf[14]=='D' 
      && buf[15]=='R') {
        this->extension = FileExtension::png;
        this->width = (buf[16]<<24) + (buf[17]<<16) + (buf[18]<<8) + (buf[19]<<0);
        this->height = (buf[20]<<24) + (buf[21]<<16) + (buf[22]<<8) + (buf[23]<<0);
        return;
    }

    this->extension = FileExtension::invalid;
    this->width = 0;
    this->height = 0;
}


string SfaceObject::createTemplate(){
	/* sface_base class

	* @brief Extract features of a face from an image with information
	*        about its position in the image.
	*
	* @tparam _Args : types face_capture or faces_collection
	* @param _args : \see face_capture or \see faces_collection content
	* @return user_template_t
	
	template <typename... _Args>
	user_template_t extract_features(_Args &&..._args);
	*/


	/* user_template class

	* @brief Get the descriptor in hexadecimal string
	* 
	* Implemented types: 
	* T = std::string (Hexadecimal String)
	* T = std::vector<char> (Array of chars)
	* @return std::string, descriptor content

	template <typename T>
	T get_descriptor() const;
	*/

    return "default";
}

bool SfaceObject::hasValidFace() {
	/* image class

	 * Create an image and put the data to it
	 * \param width     : Width size value
	 * \param height    : Height size value
	 * \param data      : data in pixels
	 * \param format    :  \ref enum class formats

	image(int width, int height, value_type *data, formats format);
	 */


	/* sface_base class
	 * @brief Detect fake image attack
	*
	* @param face_captured \ref face_capture
	* @return true : Face is real
	* @return false : Face is fake

	bool liveness_detected(const face_capture &face_captured) const;
	*/


	/* user_template class
     * User template is valid

    bool is_ok() const;
     */


	/* user_template class

     * @brief Capture some face in image
     *
     * @param img : Image object
     * @param auto_resize : Auto resize image for performance
     * @return  \see face_capture
	 
    face_capture capture(const image &img, bool auto_resize = true);
     */

    return true;
}
