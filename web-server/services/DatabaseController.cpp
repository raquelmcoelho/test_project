#include "DatabaseController.h"
#include <iostream>

using namespace std;

DatabaseController *DatabaseController::instance = 0;

DatabaseController::DatabaseController() {
}

DatabaseController::~DatabaseController() {
}

DatabaseController *DatabaseController::getInstance() {
    if(not DatabaseController::instance)
        DatabaseController::instance = new DatabaseController();

    return  DatabaseController::instance;
}

void DatabaseController::connect() {
}

bool DatabaseController::findToken(string token) {
    return true;
}