#pragma once

#include <iostream>

class SHA256Controller {
  private:
    SHA256Controller();
    ~SHA256Controller();
    static SHA256Controller *instance;
    
  public:
    static SHA256Controller *getInstance();
    static std::string generateSHA256(const std::string& str);
    static bool compareSHA256(const std::string& expected, const std::string& received);
};
