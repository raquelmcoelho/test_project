#pragma once
#include <iostream>
#include <includesface.hpp>
#include <drogon/drogon.h>

typedef enum {
    invalid = 0,
    png,
    jpg
} FileExtension;

class SfaceObject {
    friend class TemplateRouteController;

    private:
        int width;
        int height;
        bool hasFace;
        string filePath;
        FileExtension extension;
        drogon::HttpFile *fileObject;

    public:
        SfaceObject(drogon::HttpFile *file);
        SfaceObject() = delete;
        ~SfaceObject();

        void readBinaryToSetProperties();
        std::string createTemplate();
        bool hasValidFace();
};