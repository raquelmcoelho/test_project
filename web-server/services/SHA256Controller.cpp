
#include <SHA256Controller.h>
#include <string.h>
#include <iomanip>
#include <sstream>
#include <string>
#include <filesystem>
#include <openssl/sha.h>

using namespace std;

SHA256Controller *SHA256Controller::instance = 0;

SHA256Controller::SHA256Controller() {
}

SHA256Controller::~SHA256Controller() {
}

SHA256Controller *SHA256Controller::getInstance() {
    if(not SHA256Controller::instance)
        SHA256Controller::instance = new SHA256Controller();
    
    return SHA256Controller::instance;
}

string SHA256Controller::generateSHA256(const string& str)
{
    unsigned char hash[SHA256_DIGEST_LENGTH];

    SHA256_CTX sha256;
    SHA256_Init(&sha256);
    SHA256_Update(&sha256, str.c_str(), str.size());
    SHA256_Final(hash, &sha256);
    
    stringstream ss;

    for(int i = 0; i < SHA256_DIGEST_LENGTH; i++){
        ss << hex << setw(2) << setfill('0') << static_cast<int>( hash[i] );
    }
    
    return ss.str();
}

bool SHA256Controller::compareSHA256(const string& expected, const string& received)
{
    return (strcmp(expected.c_str(), received.c_str()) == 0);
}
