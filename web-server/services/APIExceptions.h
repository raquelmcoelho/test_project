#pragma once
#include <exception>
#include <drogon/HttpTypes.h>
#include <iostream>

using namespace std;
using namespace drogon;

class APIException : public exception {
    public:
        APIException(enum HttpStatusCode statusCode, string message) : statusCode(statusCode), message(message) {};
        virtual ~APIException() {};
        const char* what() const throw() { return message.c_str();};
        enum HttpStatusCode statusCode;
    private:
        string message;

};

class MultipleFilesException : public APIException {
    public:
        MultipleFilesException() : APIException(HttpStatusCode::k403Forbidden, "Must be only file") {};
};

class InvalidImageSizeException : public APIException {
    public:
        InvalidImageSizeException() : APIException(HttpStatusCode::k403Forbidden, "Invalid image size, must have a minimum size" ) {};
};

class InvalidImageFormatException : public APIException {
    public:
        InvalidImageFormatException() : APIException(HttpStatusCode::k403Forbidden, "Invalid image format, must be JPEG or PNG") {};
};

class InvalidImageContentException : public APIException {
    public:
        InvalidImageContentException() : APIException(HttpStatusCode::k403Forbidden, "Invalid image content, must have a live face") {};
};

class InvalidTokenException : public APIException {
    public:
        InvalidTokenException() : APIException(HttpStatusCode::k401Unauthorized, "Invalid token, unauthorized method") {};
};

class NoneFilesException : public APIException {
    public:
        NoneFilesException() : APIException(HttpStatusCode::k403Forbidden, "Must be some image file") {};
};

class NoneTokenException : public APIException {
    public:
        NoneTokenException() : APIException(HttpStatusCode::k401Unauthorized, "Must be some authorization token (some field named 'token')") {};
};

class BadRequestException : public APIException {
    public:
        BadRequestException() : APIException(HttpStatusCode::k400BadRequest, "Bad Request") {};
};
