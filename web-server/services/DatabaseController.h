#pragma once
#include <iostream>

class DatabaseController {
  private:
    DatabaseController();
    ~DatabaseController();
    static DatabaseController *instance;

  public:
    static DatabaseController *getInstance();
    static void connect();
    static bool findToken(string token);
};