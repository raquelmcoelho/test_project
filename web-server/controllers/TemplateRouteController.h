#pragma once

#include <drogon/HttpSimpleController.h>

using namespace drogon;

class TemplateRouteController : public drogon::HttpSimpleController<TemplateRouteController> {
	public:
		void asyncHandleHttpRequest(const HttpRequestPtr& req, std::function<void (const HttpResponsePtr &)> &&callback) override;
		bool handleParametersExceptions(MultiPartParser& formData, const HttpRequestPtr& req);
		bool validateRequest(MultiPartParser& formData, const HttpRequestPtr& req);
		bool validateNumberOfFiles(MultiPartParser& formData);
		bool validateExistenceOfToken(MultiPartParser& formData);
		bool validateImage(HttpFile file);
		bool validateToken(const std::string& token);
		std::string createTemplate(HttpFile& file);
		void response(const Json::Value& respJson, const HttpStatusCode& statusCode, std::function<void (const HttpResponsePtr &)> &&callback);
		
		PATH_LIST_BEGIN
		// list path definitions here;
		// PATH_ADD("/path", "filter1", "filter2", HttpMethod1, HttpMethod2...);
		PATH_ADD("/template", Post);
		PATH_LIST_END
};

