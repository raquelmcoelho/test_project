#include "MainRouteController.h"

void MainRouteController::asyncHandleHttpRequest(const HttpRequestPtr& req, std::function<void (const HttpResponsePtr &)> &&callback)
{

    Json::Value json;
    Json::Value array;
    Json::Value links;

    links["href"] = "http://localhost:8080/template";
    links["rel"] = "self";
    links["type"] = "POST";

    array.append(links);
    json["links"] = array;

    auto response = HttpResponse::newHttpJsonResponse(json);
    response->setBody("");
    callback(response);
}
