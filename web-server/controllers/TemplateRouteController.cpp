#include "TemplateRouteController.h"
#include "SHA256Controller.cpp"
#include "DatabaseController.cpp"
#include "SfaceObject.cpp"
#include "APIExceptions.h"
#include <iostream>
// #include <boost/gil.hpp>

using namespace drogon;
using namespace sfacelib;
using namespace std;

void TemplateRouteController::asyncHandleHttpRequest(const HttpRequestPtr& req, function<void (const HttpResponsePtr &)> &&callback) {
    MultiPartParser formData;
    Json::Value respJson;
    
    try{    
        bool canContinue = handleParametersExceptions(formData, req);
        if(not canContinue) return;

        auto file = formData.getFiles()[0];
        respJson["template"] = createTemplate(file);

        response(respJson, k200OK, std::move(callback));

    } catch (const APIException& e) {
        respJson["error"] = e.what();
        response(respJson, e.statusCode, std::move(callback));
    }
}


bool TemplateRouteController::handleParametersExceptions(MultiPartParser& formData, const HttpRequestPtr& req) {
    bool canContinue = true;

    canContinue = validateRequest(formData, req);
    if(not canContinue) return false;

    canContinue = validateNumberOfFiles(formData);
    if(not canContinue) return false;

    canContinue = validateExistenceOfToken(formData);
    if(not canContinue) return false;

    canContinue = validateImage(formData.getFiles()[0]);
    if(not canContinue) return false;

    canContinue = validateToken(formData.getParameters().at("token"));
    if(not canContinue) return false;

    return true;
}

bool TemplateRouteController::validateRequest(MultiPartParser& formData, const HttpRequestPtr& req) {
    if (formData.parse(req) != 0) {
        throw BadRequestException();
        return false;
    } 

    return true;
}

bool TemplateRouteController::validateNumberOfFiles(MultiPartParser& formData) {
    if(formData.getFiles().size() != 1) {
        if (formData.getFiles().size() > 1)
            throw MultipleFilesException();
        else 
            throw NoneFilesException();
        return false;
    }

    return true;
}

bool TemplateRouteController::validateExistenceOfToken(MultiPartParser& formData) {
    if(not formData.getParameters().count( "token" )) {
        throw NoneTokenException();
        return false;
    }

    return true;
}

bool TemplateRouteController::validateImage(HttpFile file) {    
    SfaceObject sfaceObject(&file);
    
    if(sfaceObject.extension == FileExtension::invalid) {
        throw InvalidImageFormatException();
        return false;
    }

    else if(not (sfaceObject.width && sfaceObject.height)) {
        throw InvalidImageSizeException();
        return false;
    }


    return true;
}


bool TemplateRouteController::validateToken(const string& token) {
    if(not DatabaseController::findToken(token)) {
        throw InvalidTokenException();
        return false;
    }

    return true;
}

string TemplateRouteController::createTemplate(HttpFile& file) {
    SfaceObject sfaceObject(&file);

    if(not sfaceObject.hasValidFace()){
        throw InvalidImageContentException();
        return "";
    }

    return sfaceObject.createTemplate();
}

void TemplateRouteController::response(const Json::Value& respJson, const HttpStatusCode& statusCode, function<void (const HttpResponsePtr &)> &&callback) {
    auto response = HttpResponse::newHttpJsonResponse(respJson);
    response->setStatusCode(statusCode);
    callback(response);
}
